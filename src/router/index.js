import Vue from 'vue';
import Router from 'vue-router';

import _ from 'lodash';
import { routes } from './routes';
import { store } from '../vuex/store';

Vue.use(Router);

const router = new Router({
  routes,
  mode: 'history',
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && _.isEmpty(store.getters.currentUser)) {
    store.commit('TOGGLE_LOGIN_MODAL');
  } else {
    next();
  }
});

export default router;
